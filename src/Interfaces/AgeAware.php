<?php namespace Fenix440\Model\Age\Interfaces;
use Fenix440\Model\Age\Exceptions\InvalidAgeException;

/**
 * Interface AgeAware
 *
 * A component must be aware of Age
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Age\Interfaces
 */
interface AgeAware {


    /**
     * Set age for given component
     *
     * @param int $age Age for given component
     * @return void
     * @throws InvalidAgeException If age is invalid
     *
     */
    public function setAge($age);

    /**
     * Get Age
     *
     * @return int|null
     */
    public function getAge();

    /**
     * Get default age
     *
     * @return int|null
     */
    public function getDefaultAge();

    /**
     * Validates if age is valid
     *
     * @param mixed $age Age for given component
     * @return bool true/false
     */
    public function isAgeValid($age);

    /**
     * Checks if age is set
     * @return true/false
     */
    public function hasAge();

    /**
     * Checks if default age is set
     * @return mixed
     */
    public function hasDefaultAge();


}