<?php  namespace Fenix440\Model\Age\Exceptions; 

/**
 * Class InvalidAgeException
 *
 * Throws invalid exception if age is invalid
 *
 * @package      Fenix440\Model\Age\Exceptions 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
class InvalidAgeException extends \InvalidArgumentException{

}