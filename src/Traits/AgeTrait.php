<?php  namespace Fenix440\Model\Age\Traits;
use Aedart\Validate\Number\Integer\UnsignedIntegerValidator;
use Fenix440\Model\Age\Exceptions\InvalidAgeException;


/**
 * Trait AgeTrait
 *
 * @see AgeAware
 *
 * @package      Fenix440\Model\Age\Traits 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
trait AgeTrait {

    /**
     * Age for given component
     * @var null|int
     */
    protected $age=null;

    /**
     * Set age for given component
     *
     * @param int $age Age for given component
     * @return void
     * @throws InvalidAgeException If age is invalid
     */
    public function setAge($age){
        if(!$this->isAgeValid($age))
            throw new InvalidAgeException(sprintf('Age "%s" is invalid',$age));

        $this->age=(int)$age;
    }

    /**
     * Get Age
     *
     * @return int|null
     */
    public function getAge(){
        if(!$this->hasAge() && $this->hasDefaultAge())
            $this->setAge($this->getDefaultAge());
        return $this->age;
    }

    /**
     * Get default age
     *
     * @return int|null
     */
    public function getDefaultAge(){
        return null;
    }

    /**
     * Validates if age is valid
     *
     * @param mixed $age Age for given component
     * @return bool true/false
     */
    public function isAgeValid($age){
        return UnsignedIntegerValidator::isValid($age);
    }

    /**
     * Checks if age is set
     * @return true/false
     */
    public function hasAge(){
        return (!is_null($this->age))? true:false;
    }

    /**
     * Checks if default age is set
     * @return mixed
     */
    public function hasDefaultAge(){
        return (!is_null($this->getDefaultAge()))? true:false;
    }


}