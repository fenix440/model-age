<?php
use Fenix440\Model\Age\Traits\AgeTrait;
use Fenix440\Model\Age\Interfaces\AgeAware;

/**
 * Class AgeTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Age\Traits\AgeTrait
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
class AgeTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Age\Interfaces\AgeAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Age\Traits\AgeTrait');
    }

    /************************************************************************
     * Actual tests
     ***********************************************************************/


    /**
     * @test
     * @covers  ::setAge
     * @covers  ::getAge
     * @covers  ::getDefaultAge
     * @covers  ::hasDefaultAge
     * @covers  ::hasAge
     * @covers  ::isAgeValid
    */
    public function setAndGetAge(){
        $trait = $this->getTraitMock();
        $age = 23;
        $trait->setAge($age);

        $this->assertSame($age,$trait->getAge(),'Invalid age');
    }

    /**
     * @test
     * @covers  ::setAge
     * @covers  ::isAgeValid
     * @expectedException \Fenix440\Model\Age\Exceptions\InvalidAgeException
    */
    public function setInvalidAge()
    {
        $trait = $this->getTraitMock();
        $age = 232.2;

        $trait->setAge($age);
    }

    /**
     * @test
     * @covers  ::setAge
     * @covers  ::isAgeValid
     * @expectedException \Fenix440\Model\Age\Exceptions\InvalidAgeException
     */
    public function setAnotherInvalidAge()
    {
        $trait = $this->getTraitMock();
        $age = -23;

        $trait->setAge($age);
    }

    /**
     * @test
     * @covers  ::getDefaultAge
    */
    public function checkDefaultAge()
    {
        $trait=$this->getTraitMock();

        $this->assertNull($trait->getDefaultAge(),'Default age is not null!');
    }


    /**
     * @test
     * @covers  ::hasDefaultAge
    */
    public function checkHasDefaultAge()
    {
        $trait = $this->getTraitMock();

        $this->assertFalse($trait->hasDefaultAge(),'Default age is set!');
    }


}